import { Component } from '@angular/core';
import { Platform } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import * as firebase from 'firebase';
import { TabsPage } from '../pages/tabs/tabs';

//Firebase
const config = {
  apiKey: "AIzaSyDELAWmBtqnwj1LSZ6dR_w_BNLqwE6eS2Q",
  authDomain: "radio-palavraweb.firebaseapp.com",
  databaseURL: "https://radio-palavraweb.firebaseio.com",
  projectId: "radio-palavraweb",
  storageBucket: "radio-palavraweb.appspot.com",
  messagingSenderId: "325870206264"
};

@Component({
  templateUrl: 'app.html'
})


export class MyApp {
  rootPage:any = TabsPage;


  constructor(platform: Platform, statusBar: StatusBar, splashScreen: SplashScreen) {
    platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      statusBar.styleDefault();
      splashScreen.hide();
    });

    //Firebase
    firebase.initializeApp(config);
  }
}

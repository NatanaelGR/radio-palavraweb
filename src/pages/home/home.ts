import { Component, OnInit } from '@angular/core';
import { NavController } from 'ionic-angular';
import { AnaliticsProvider } from './../../providers/analitics/analitics';
import * as firebase from 'Firebase';

export const snapshotToArray = snapshot => {
  let returnArr = [];

  snapshot.forEach(childSnapshot => {
      let item = childSnapshot.val();
      //item.key = childSnapshot.key;
      returnArr.push(item);
  });

  return returnArr;
};

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage{


  url:string;
  usersOnline:any = 0;
  stream:any;
  promise:any;
  rooms = [];
  ref = firebase.database().ref('analytics');
  showUsers:boolean;
  playerListening: boolean;
  

  constructor(public navCtrl: NavController, private live:AnaliticsProvider) {
    this.url = "http://c1.fabricahost.com.br:8044/live";   
    this.stream = new Audio(this.url); 
    this.showUsers = false; 
    this.playerListening = false;
      
  }

  play(){
    this.loadOnline(); 
    if(this.playerListening === false){
      this.playerListening = true;
      this.stream = new Audio(this.url); 
      this.stream.play();
      this.promise = new Promise((resolve,reject) => {
         this.stream.addEventListener('playing', () => {       
            this.loadOnline(); 
            this.setOnline(); 
            this.showUsers = true;      
            resolve(true);   
          }
           
        );
    
         this.stream.addEventListener('error', () => { 
            this.playerListening = false;        
            reject(false);
         });
       });
       
      return this.promise;
    }
  }

  

  pause(){
    if(this.playerListening){      
      this.loadOnline();    
      this.showUsers = false;
      this.stream.pause();
      this.setOffline(); 
      this.playerListening = false;
    }
    
  }

  loadOnline(){
    this.ref.on('value', resp => {
      this.rooms = [];
      this.rooms = snapshotToArray(resp);
      this.usersOnline = this.rooms[1];       
    });
  }

  setOnline(){  
    this.loadOnline();  
    this.ref.child('online').set(parseInt(this.usersOnline) + 1);
  }

  setOffline(){        
    if(this.usersOnline > 0){
      this.ref.child('online').set(parseInt(this.usersOnline) - 1);
      this.showUsers = false;
    }
    
  }




   

}

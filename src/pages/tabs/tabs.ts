import { Component } from '@angular/core';

import { ContactPage } from '../contact/contact';
import { HomePage } from '../home/home';
import { ProgramPage } from '../program/program';
import { VideosPage } from '../videos/videos';

@Component({
  templateUrl: 'tabs.html'
})
export class TabsPage {

  tab1Root = HomePage;
  tab2Root = ProgramPage;
  tab3Root = VideosPage;
  tab4Root = ContactPage;

  constructor() {

  }
}
